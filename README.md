````
 ██████  ██████  ███    ███ ██████   █████   ██████ ████████ ███████ ███    ███
██      ██    ██ ████  ████ ██   ██ ██   ██ ██         ██    ██      ████  ████
██      ██    ██ ██ ████ ██ ██████  ███████ ██         ██    █████   ██ ████ ██
██      ██    ██ ██  ██  ██ ██      ██   ██ ██         ██    ██      ██  ██  ██
 ██████  ██████  ██      ██ ██      ██   ██  ██████    ██    ███████ ██      ██
````
## compactem: build accurate small models!


Hi!

For documentation:

* visit the online documentation [here](https://compactem.readthedocs.io/en/latest/index.html). OR,
* view ``docs/_build/html/index.html`` in your local repo.

License: [Apache-v2](https://bitbucket.org/aghose/compactem/src/master/compactem/LICENSE-2.0.txt)