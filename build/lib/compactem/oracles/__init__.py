"""
Various oracle learners.
"""

from .oracle_learners import get_calibrated_gbm, get_calibrated_rf