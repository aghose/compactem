import logging, sys
import numpy as np, os, pickle
from matplotlib import pyplot as plt
import seaborn as sns;
sns.set()
from compactem.oracles import get_calibrated_rf, get_calibrated_gbm
from compactem.utils.data_format import DataInfo
from compactem.main import compact_using_oracle
from compactem.model_builder import RandomForest, GradientBoostingModel, LinearProbabilityModel
from compactem.utils.output_processors import Result
from sklearn.metrics import f1_score
from sklearn.datasets import load_digits
from sklearn.model_selection import train_test_split

LOGFILE = r'output/experiment_with_oracle.log'


def setup_logger():
    """
    set up the logger to log in a file as well as stdout
    :return:
    """
    logFormatter = logging.Formatter("%(asctime)s [%(process)d] [%(threadName)s] [%(levelname)-5.5s] "
                                     "[%(filename)s:%(lineno)d] [%(funcName)s]"
                                     "  %(message)s")
    rootLogger = logging.getLogger()
    rootLogger.setLevel(logging.INFO)

    fileHandler = logging.FileHandler(LOGFILE, mode='w')
    fileHandler.setFormatter(logFormatter)
    rootLogger.addHandler(fileHandler)

    consoleHandler = logging.StreamHandler(sys.stdout)
    consoleHandler.setFormatter(logFormatter)
    rootLogger.addHandler(consoleHandler)

setup_logger()


def demo_sample_points():
    import numpy as np, os, pickle
    from matplotlib import pyplot as plt
    import seaborn as sns; sns.set()
    from compactem.oracles import get_calibrated_rf
    from compactem.utils.data_format import DataInfo
    from compactem.main import compact_using_oracle
    from compactem.model_builder import DecisionTree
    from sklearn.metrics import f1_score

    def true_classifier(X):
        # centre, radius
        c, r = np.array([0.9, 0.5]), 0.1
        y = np.zeros(np.shape(X)[0])
        y[X[:, 0] <= 0.5] = 1
        y[np.sum(np.power(X - np.tile(c, (np.shape(X)[0], 1)), 2), axis=1) <= r ** 2] = 1
        return y.astype(int)

    task_dir = r'output/sample_test'
    N, T = 5000, 1000
    X = np.random.random((N, 2))
    y = true_classifier(X)

    fig_scatter = plt.figure(figsize=(20, 8))
    fig_kde = plt.figure(figsize=(15, 8))
    ax = fig_scatter.add_subplot(131)
    sns.scatterplot(x=X[:, 0], y=X[:, 1], hue=y, ax=ax, legend=True)
    ax.set_title(f"dataset")

    dataset_info = DataInfo("my_data", (X, y), list(range(1, 7)), evals=T)

    results = compact_using_oracle(datasets_info=dataset_info,
                                   model_builder_class=DecisionTree,
                                   oracle=get_calibrated_gbm,
                                   task_dir=task_dir, overwrite=True,
                                   save_optimal_sample=True,
                                   max_sample_size=50, min_sample_size=20, min_sample_size_per_dist=10)

    result_obj = Result(task_dir)
    result_obj.process_results()
    results= result_obj.read_processed_results()
    complexities = [results['complexity'].min(), results['complexity'].max()]

    # Lets generate a meshgrid to visualize the generalization obtained by a classifier. We'll use this as our test set
    # as well.

    N_plot_per_axis = 500
    x_plot = np.linspace(0, 1, N_plot_per_axis)
    xv, yv = np.meshgrid(x_plot, x_plot)

    X_plot = np.hstack((np.ravel(xv).reshape(-1, 1), np.ravel(yv).reshape(-1, 1)))
    y_plot_actual = true_classifier(X_plot)

    for comp_idx, comp in enumerate(complexities):
        ax = fig_scatter.add_subplot(132 + comp_idx)

        ax_k = fig_kde.add_subplot(121 + comp_idx)
        ax_k.set_xlim(0, 1)
        ax_k.set_ylim(0, 1)

        t1, info = result_obj.get_optimal_sample("my_data", comp)
        model_file_path = os.path.join(task_dir, info['best_model_path'])
        with open(model_file_path, 'rb') as f_model:
            best_model = pickle.load(f_model)
            best_model = best_model[0]

        # predict on the meshgrid points
        y_plot = best_model.predict(X_plot)
        f1 = f1_score(y_plot_actual, y_plot, average='macro')
        # this visualizes the generalization
        sns.scatterplot(x=X_plot[:, 0], y=X_plot[:, 1], hue=y_plot, ax=ax, legend=False, alpha=0.5, s=1)

        # lets get the sampled points
        X_sample, y_sample = t1[0]

        # Create a scattereplot to show the actual sample
        sns.scatterplot(x=X_sample[:, 0], y=X_sample[:, 1], hue=y_sample, ax=ax, legend=False, edgecolor='k',
                        linewidth=0.7, s=50)
        # and a kde plot to show the sampling distribution.
        sns.kdeplot(x=X_sample[:, 0], y=X_sample[:, 1], ax=ax_k, fill=True, levels=10, hue=y_sample)

        ax.set_title(f"DT, depth={comp}. F1_macro: {f1: .2f}")
        ax_k.set_title(f"DT, depth={comp}. F1_macro: {f1: .2f}")

    fig_scatter.savefig(os.path.join(task_dir, 'summary', 'samples.png'), bbox_inches='tight')
    fig_kde.savefig(os.path.join(task_dir, 'summary', 'samples_kde.png'), bbox_inches='tight')
    plt.close(fig_scatter)
    plt.close(fig_kde)

    # lets also try the approximate sampler
    fig_approx = plt.figure(figsize=(15, 8))
    for comp_idx, comp in enumerate(complexities):
        ax = fig_approx.add_subplot(121 + comp_idx)

        sample_idxs = result_obj.get_approx_sampled_indices("my_data", comp)
        X_sample, y_sample = X[sample_idxs], y[sample_idxs]
        sns.scatterplot(x=X_sample[:, 0], y=X_sample[:, 1], hue=y_sample, ax=ax, legend=False, edgecolor='k',
                        linewidth=0.7, s=50)
    fig_approx.savefig(os.path.join(task_dir, 'summary', 'approx_samples.png'), bbox_inches='tight')
    plt.close(fig_approx)

    print(result_obj.get_compaction_profile())


def demo_gbm():
    N, T = 1000, 5
    X, y = load_digits(return_X_y=True)

    # use only N points, for quicker experiments
    X, _, y, _ = train_test_split(X, y, train_size=N, stratify=y, random_state=0)
    dataset_info = DataInfo("digits", (X, y), [(2, 3), (3, 5)], evals=T)

    aggr_results_df = compact_using_oracle(datasets_info=dataset_info,
                                           model_builder_class=GradientBoostingModel,
                                           oracle=get_calibrated_gbm,
                                           task_dir=r'output/gbm', overwrite=True)


def demo_rf():
    N, T = 1000, 5
    X, y = load_digits(return_X_y=True)

    # use only N points, for quicker experiments
    X, _, y, _ = train_test_split(X, y, train_size=N, stratify=y, random_state=0)
    dataset_info = DataInfo("digits", (X, y), [(3, 4), (2, 7)], evals=T)

    aggr_results_df = compact_using_oracle(datasets_info=dataset_info,
                                           model_builder_class=RandomForest,
                                           oracle=get_calibrated_gbm,
                                           task_dir=r'output/rf', overwrite=True)


def demo_lpm():
    N, T = 1000, 5
    X, y = load_digits(return_X_y=True)

    # use only N points, for quicker experiments
    X, _, y, _ = train_test_split(X, y, train_size=N, stratify=y, random_state=0)
    # oracle_model = CalibratedRandomForest(params_range={'n_estimators': (5, 10)})
    dataset_info = DataInfo("digits", (X, y), [2, 5], evals=T)

    aggr_results_df = compact_using_oracle(datasets_info=dataset_info,
                                           model_builder_class=LinearProbabilityModel,
                                           oracle=get_calibrated_gbm,
                                           oracle_params={'num_boosting_rounds':200, 'max_depth':5, 'learning_rate': 0.1},
                                           task_dir=r'output/lpm', overwrite=True)


if __name__== "__main__":
    demo_sample_points()
    # demo_gbm()
    # demo_rf()
    # demo_lpm()