

<!DOCTYPE html>
<html class="writer-html5" lang="en" >
<head>
  <meta charset="utf-8">
  
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  
  <title>Overview &mdash; compactem 0.9.5 documentation</title>
  

  
  <link rel="stylesheet" href="_static/css/theme.css" type="text/css" />
  <link rel="stylesheet" href="_static/pygments.css" type="text/css" />

  
  
  
  

  
  <!--[if lt IE 9]>
    <script src="_static/js/html5shiv.min.js"></script>
  <![endif]-->
  
    
      <script type="text/javascript" id="documentation_options" data-url_root="./" src="_static/documentation_options.js"></script>
        <script src="_static/jquery.js"></script>
        <script src="_static/underscore.js"></script>
        <script src="_static/doctools.js"></script>
        <script src="_static/language_data.js"></script>
        <script async="async" src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.7/latest.js?config=TeX-AMS-MML_HTMLorMML"></script>
    
    <script type="text/javascript" src="_static/js/theme.js"></script>

    
    <link rel="author" title="About these documents" href="about.html" />
    <link rel="index" title="Index" href="genindex.html" />
    <link rel="search" title="Search" href="search.html" /> 
</head>

<body class="wy-body-for-nav">

   
  <div class="wy-grid-for-nav">
    
    <nav data-toggle="wy-nav-shift" class="wy-nav-side">
      <div class="wy-side-scroll">
        <div class="wy-side-nav-search" >
          

          
            <a href="index.html" class="icon icon-home" alt="Documentation Home"> compactem
          

          
          </a>

          
            
            
          

          
<div role="search">
  <form id="rtd-search-form" class="wy-form" action="search.html" method="get">
    <input type="text" name="q" placeholder="Search docs" />
    <input type="hidden" name="check_keywords" value="yes" />
    <input type="hidden" name="area" value="default" />
  </form>
</div>

          
        </div>

        
        <div class="wy-menu wy-menu-vertical" data-spy="affix" role="navigation" aria-label="main navigation">
          
            
            
              
            
            
              <p class="caption"><span class="caption-text">Documentation</span></p>
<ul>
<li class="toctree-l1"><a class="reference internal" href="quickstart.html">Quickstart</a></li>
<li class="toctree-l1"><a class="reference internal" href="key_ideas.html">Key Ideas</a></li>
<li class="toctree-l1"><a class="reference internal" href="usage.html">Usage</a></li>
<li class="toctree-l1"><a class="reference internal" href="api_reference.html">API Reference</a></li>
<li class="toctree-l1"><a class="reference internal" href="cite_us.html">Cite Us</a></li>
</ul>
<p class="caption"><span class="caption-text">General</span></p>
<ul>
<li class="toctree-l1"><a class="reference internal" href="LICENSE-2.0.html">License</a></li>
<li class="toctree-l1"><a class="reference internal" href="faq.html">FAQ</a></li>
<li class="toctree-l1"><a class="reference internal" href="about.html">About</a></li>
</ul>

            
          
        </div>
        
      </div>
    </nav>

    <section data-toggle="wy-nav-shift" class="wy-nav-content-wrap">

      
      <nav class="wy-nav-top" aria-label="top navigation">
        
          <i data-toggle="wy-nav-top" class="fa fa-bars"></i>
          <a href="index.html">compactem</a>
        
      </nav>


      <div class="wy-nav-content">
        
        <div class="rst-content">
        
          















<div role="navigation" aria-label="breadcrumbs navigation">

  <ul class="wy-breadcrumbs">
    
      <li><a href="index.html" class="icon icon-home"></a> &raquo;</li>
        
      <li>Overview</li>
    
    
      <li class="wy-breadcrumbs-aside">
        
            
            <a href="_sources/readme_link.rst.txt" rel="nofollow"> View page source</a>
          
        
      </li>
    
  </ul>

  
  <hr/>
</div>
          <div role="main" class="document" itemscope="itemscope" itemtype="http://schema.org/Article">
           <div itemprop="articleBody">
            
  <div class="section" id="overview">
<h1>Overview<a class="headerlink" href="#overview" title="Permalink to this headline">¶</a></h1>
<p>This library implements a set of algorithms to create <strong>compact models</strong>: smaller versions of models with accuracy
similar to their original version. The strategy is to use a much more powerful model to guide the training of
a small model.</p>
<p>Here’s a typical use-case:</p>
<ul class="simple">
<li><p>You are interested in building a Decision Tree with <code class="docutils literal notranslate"><span class="pre">depth</span> <span class="pre">&lt;=5</span></code>. We informally refer to this as the <em>small model</em>.</p></li>
<li><p>First, you create a powerful <em>oracle</em> model. Say, Gradient Boosted Decision Trees.</p></li>
<li><p>Then, you use this to train the Decision Tree. The benefit is the small model is likely to have greater accuracy than if you had trained it normally, i.e., without the oracle.</p></li>
</ul>
<p>When might you want this?</p>
<ul class="simple">
<li><p><strong>Interpretability</strong>: if you want a model to be interpretable by humans, it typically implies it cannot be large. This library allows you to create compact versions of your preferred model family/library. This was our motivation.</p></li>
<li><p><strong>Compressed Models</strong>: you might want small models because of constraints on your execution environment, like an edge device.</p></li>
</ul>
<p>An important difference with some other approaches is that here you <strong>BYOM</strong> - <em>bring your own model</em>. We don’t
prescribe a specific kind of Decision Tree, or a Linear Model, etc. You can pair up <em>any</em> oracle with <em>any</em> model.
In other words, the algorithms are <strong>model-agnostic</strong>. Sure, the <em>library</em> supports certain
models, but this is for convenience; you can just as well write your own models and oracles, and have the library
use them (see <a class="reference internal" href="usage.html#custom-models"><span class="std std-ref">Writing Custom Oracles and Models</span></a>).</p>
<p>For ex, any small model in the following table can be paired with any oracle.</p>
<table class="docutils align-default" id="id1">
<caption><span class="caption-text">Examples of small models and oracles</span><a class="headerlink" href="#id1" title="Permalink to this table">¶</a></caption>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<thead>
<tr class="row-odd"><th class="head"><p>small model</p></th>
<th class="head"><p>oracle</p></th>
</tr>
</thead>
<tbody>
<tr class="row-even"><td><p>Decision Tree</p></td>
<td><p>Gradient Boosted Decision Trees</p></td>
</tr>
<tr class="row-odd"><td><p>Linear Probability Model</p></td>
<td><p>Random Forest</p></td>
</tr>
<tr class="row-even"><td><p>Random Forest (RF)</p></td>
<td></td>
</tr>
<tr class="row-odd"><td><p>Gradient Boosted Decision Trees (GBDT)</p></td>
<td></td>
</tr>
</tbody>
</table>
<p>Did you notice RF and GBDT figures in both columns? Yes, you can use a larger model as an oracle to train a small model
of the <em>same model family</em>!</p>
<p>This seems to work on a variety of datasets across different model combinations;
check out the <a class="reference internal" href="key_ideas.html#what-to-expect"><span class="std std-ref">What to Expect</span></a> section.</p>
<p>There are really just only two rules:</p>
<ul class="simple">
<li><p>The oracle must have a accuracy higher than the small model you want to train.</p></li>
<li><p>The oracle must be <em>probabilistic</em>, i.e., it must produce probabilities for its label predictions.</p></li>
</ul>
<div class="admonition tip">
<p class="admonition-title">Tip</p>
<p>Even if you have a preferred model training algorithm for creating interpretable or small models,
you can run it through our library <em>anyway</em> to see if you can obtain an <em>even</em> smaller model with similar accuracy.</p>
</div>
<p>Please <a class="reference internal" href="cite_us.html"><span class="doc">Cite Us</span></a> if you use the software. The software is distributed under the <em>Apache V2.0</em> <a class="reference internal" href="LICENSE-2.0.html"><span class="doc">License</span></a>.</p>
</div>


           </div>
           
          </div>
          <footer>
  

  <hr/>

  <div role="contentinfo">
    <p>
        
        &copy; Copyright 2020, Abhishek Ghose

    </p>
  </div>
    
    
    
    Built with <a href="http://sphinx-doc.org/">Sphinx</a> using a
    
    <a href="https://github.com/rtfd/sphinx_rtd_theme">theme</a>
    
    provided by <a href="https://readthedocs.org">Read the Docs</a>. 

</footer>

        </div>
      </div>

    </section>

  </div>
  

  <script type="text/javascript">
      jQuery(function () {
          SphinxRtdTheme.Navigation.enable(true);
      });
  </script>

  
  
    
   

</body>
</html>