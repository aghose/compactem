******
About
******

I am `Abhishek Ghose <https://www.linkedin.com/in/abhishek-ghose-36197624/>`_, author and current maintainer of this
library. Much of this work grew out my PhD research done at the Department
of `Computer Science and Engineering <https://www.cse.iitm.ac.in/>`_ at Indian Institute of Technology (IIT), Madras.
This work was made largely possible by the support of my PhD advisor
`Dr. Balaraman Ravindran <https://www.cse.iitm.ac.in/~ravi/>`_, and
the `Robert Bosch Centre for Data Science and Artificial Intelligence <https://rbcdsai.iitm.ac.in/>`_ (RBCDSAI),
IIT Madras.
