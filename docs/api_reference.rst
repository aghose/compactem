
***************
API Reference
***************
This section is useful only if you want to modify or extend the code. As a user, you don't need to
know these details.

Some of the inconsistencies you might see are a consequence of:

* A lot of this codebase was ported from Python 2.
* Much of the original codebase was geared towards long-running research experiments.
* The codebase itself evolved intermittently as I explored different ideas.

I am still in the process of refactoring.

Here's a list of the various packages to begin exploring:

.. autosummary::
   :toctree: _autosummary

   compactem
   compactem.core
   compactem.oracles
   compactem.model_builder
   compactem.utils

There's also a :ref:`modindex` for reference.
