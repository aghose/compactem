
********
Cite Us
********

If you use this library, please include the following citations::

    @software{compactem2020,
        author       = {Abhishek Ghose},
        title        = {compactem},
        month        = Nov,
        year         = 2020,
        publisher    = {PyPI},
        version      = {latest},
        url          = {https://pypi.org/project/compactem/}
     }


    @article{DBLP:journals/corr/abs-1906-06852,
      author    = {Abhishek Ghose and
                   Balaraman Ravindran},
      title     = {Learning Interpretable Models Using an Oracle},
      journal   = {CoRR},
      volume    = {abs/1906.06852},
      year      = {2019},
      url       = {http://arxiv.org/abs/1906.06852},
      archivePrefix = {arXiv},
      eprint    = {1906.06852},
      timestamp = {Mon, 24 Jun 2019 17:28:45 +0200},
      biburl    = {https://dblp.org/rec/journals/corr/abs-1906-06852.bib},
      bibsource = {dblp computer science bibliography, https://dblp.org}
    }

The software is distributed under the *Apache V2.0* license.