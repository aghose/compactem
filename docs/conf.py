# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import sphinx_rtd_theme
import os
import sys
sys.path.insert(0, os.path.abspath('../'))
modindex_common_prefix = ['compactem.']
add_module_names = False
autoclass_content = 'both'
napoleon_use_param = True
run_codes=False
QUICKSTART_FILE = 'quickstart.rst'
QUICKSTART_FILE_BACKUP = 'quickstart.rst.bak'

if run_codes:
    # directly read code and add to documentation
    # add the output from running it, to the documentation as well
    sys.path.append("..")
    import quickstart
    import inspect, re, io
    from contextlib import redirect_stdout
    quickstart_code = inspect.getsource(quickstart.demo_oracle_transfer)
    # remove the first line - this is the function name
    quickstart_code = "".join(list(quickstart_code)[quickstart_code.index("\n") + 1:])
    with open(QUICKSTART_FILE) as f:
        existing_content = f.read()
    with open(QUICKSTART_FILE_BACKUP, 'w') as f:
        f.write(existing_content)
    comment_marker_start = r"..\s+_CODE_START"
    comment_marker_end = r"..\s+_CODE_END"
    m_start = re.search(comment_marker_start, existing_content)
    m_end = re.search(comment_marker_end, existing_content)
    if m_start and m_end:
        print("Found comment markers.")
        pre_str = existing_content[:m_start.end(0)]
        post_str = existing_content[m_end.start(0):]
        code_str = "\n\n.. code:: python\n\n" + quickstart_code
        new_str = pre_str + code_str + post_str

        with open(QUICKSTART_FILE, 'w') as f:
            f.write(new_str)

    f = io.StringIO()
    with redirect_stdout(f):
        quickstart.demo_oracle_transfer()
    out = f.getvalue()
    result_start_str = "Result summary:"
    result_start_idx = out.index(result_start_str)
    result_table_op = out[result_start_idx + len(result_start_str):]
    lines = result_table_op.split("\n")
    data_lines = ""
    for line in lines:
        if line.strip() != "":
            data_lines += re.sub(r'\s+', ',', line.rstrip()) + "\n"
    print(data_lines)
    with open(r'sample_op.csv', 'w') as f_sample:
        f_sample.write(data_lines)


# -- Project information -----------------------------------------------------

project = 'compactem'
copyright = '2020, Abhishek Ghose'
author = 'Abhishek Ghose'

# The full version, including alpha/beta/rc tags
release = '0.9.5'


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions =  ['sphinx.ext.autodoc', 'sphinx.ext.napoleon', 'sphinx.ext.autosummary', "sphinx_rtd_theme",
               'sphinx_autodoc_typehints']

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']


# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = "sphinx_rtd_theme"

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']