.. compactem documentation master file, created by
   sphinx-quickstart on Sat Oct 24 13:49:02 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

########################################
compactem: build accurate small models!
########################################



.. include:: ../README.rst

.. toctree::
   :caption: Documentation
   :maxdepth: 4

   quickstart
   key_ideas
   usage
   api_reference
   cite_us

.. toctree::
   :caption: General
   :maxdepth: 4

   LICENSE-2.0
   faq
   about





Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
