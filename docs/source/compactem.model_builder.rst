compactem.model\_builder package
================================

.. automodule:: compactem.model_builder
   :members:
   :undoc-members:
   :show-inheritance:

Submodules
----------

compactem.model\_builder.DecisionTreeModelBuilder module
--------------------------------------------------------

.. automodule:: compactem.model_builder.DecisionTreeModelBuilder
   :members:
   :undoc-members:
   :show-inheritance:

compactem.model\_builder.GradientBoostingClassifier module
----------------------------------------------------------

.. automodule:: compactem.model_builder.GradientBoostingClassifier
   :members:
   :undoc-members:
   :show-inheritance:

compactem.model\_builder.LarsAndRidge module
--------------------------------------------

.. automodule:: compactem.model_builder.LarsAndRidge
   :members:
   :undoc-members:
   :show-inheritance:

compactem.model\_builder.LinearProbabilityModel module
------------------------------------------------------

.. automodule:: compactem.model_builder.LinearProbabilityModel
   :members:
   :undoc-members:
   :show-inheritance:

compactem.model\_builder.RandomForestClassifier module
------------------------------------------------------

.. automodule:: compactem.model_builder.RandomForestClassifier
   :members:
   :undoc-members:
   :show-inheritance:

compactem.model\_builder.base\_model module
-------------------------------------------

.. automodule:: compactem.model_builder.base_model
   :members:
   :undoc-members:
   :show-inheritance:
