compactem.oracles package
=========================

.. automodule:: compactem.oracles
   :members:
   :undoc-members:
   :show-inheritance:

Submodules
----------

compactem.oracles.oracle\_learners module
-----------------------------------------

.. automodule:: compactem.oracles.oracle_learners
   :members:
   :undoc-members:
   :show-inheritance:
