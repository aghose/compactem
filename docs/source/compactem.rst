compactem package
=================

.. automodule:: compactem
   :members:
   :undoc-members:
   :show-inheritance:

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   compactem.core
   compactem.model_builder
   compactem.oracles
   compactem.utils

Submodules
----------

compactem.main module
---------------------

.. automodule:: compactem.main
   :members:
   :undoc-members:
   :show-inheritance:
